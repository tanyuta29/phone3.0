public abstract class Phone {
    private String name;
    private String model;
    private int storageVolume;
    private int RAMvolume;

    public Phone(String name, String model, int storageVolume, int RAMvolume) {
        this.model = model;
        this.name = name;
        this.storageVolume = storageVolume;
        this.RAMvolume = RAMvolume;
    }

    public Phone() {
    }

    public String getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel() {
        this.model = model;
    }

    public int getStorageValume() {
        return storageVolume;
    }

    public void setStorageVolume() {
        this.storageVolume = storageVolume;
    }

    public int getRAMvolume() {
        return RAMvolume;
    }

    public void setRAMvolume() {
        this.RAMvolume = RAMvolume;
    }

    public String toString() {
        return "model" + model + " " + name + " " + "storage volume:" + storageVolume + " " + "ram volume:" + RAMvolume;
    }
}

