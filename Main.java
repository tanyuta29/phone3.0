public class Main {
    public static void main(String[] args) {
        NokiaPhone nokiaPhone = new NokiaPhone();
        nokiaPhone.specialNumber = 768;
        System.out.println("Nokia special number is " + nokiaPhone.getSpecialNumber());
        SamsungPhone samsungPhone = new SamsungPhone();
        samsungPhone.versia = "l";
        System.out.println("Samsung version is: version " + samsungPhone.versia);
        Phone phone = new Phone(){} ;
        String phoneName = phone.getName();
        System.out.println("Phone name is " + phoneName);
    }
}