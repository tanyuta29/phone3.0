public class NokiaPhone extends Phone implements PhoneConnection {
    public int specialNumber;

    public NokiaPhone() {

    }


    public int getSpecialNumber() {
        return specialNumber;
    }

    public void call() {
        System.out.println("Chose number");
    }

    public void sandMessage() {
        System.out.println("Write message");
    }

    public NokiaPhone(String name, String model, int storageVolume, int RAMvolume, int specialNumber) {
        super(name, model, storageVolume, RAMvolume);
        this.specialNumber = specialNumber;
    }

    public void setStorageVolume() {
        super.setStorageVolume();
    }

    public void setRAMvolume() {
        super.setRAMvolume();
    }

    public void setName() {
        super.setName();
    }

    public void setModel() {
        super.setModel();
    }

    public String getName() {
        return super.getName();
    }

    public String getModel() {
        return super.getModel();
    }

    public int getStorageValume() {
        return super.getStorageValume();
    }

    public int getRAMvolume() {
        return super.getRAMvolume();
    }

    public void setSpecialNumber(int specialNumber) {
        this.specialNumber = specialNumber;
    }

    public String toString() {
        return "Nokia phone" + "special number:" + specialNumber;
    }
}