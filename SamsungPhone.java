public class SamsungPhone extends Phone implements PhoneConnection, PhoneMedia {
    public String versia;

    public SamsungPhone(String name, String model, int storageVolume, int RAMvolume, String versia) {
        super(name, model, storageVolume, RAMvolume);
        this.versia = versia;
    }

    public SamsungPhone() {

    }

    public int getRAMvolume() {
        return super.getRAMvolume();
    }

    public int getStorageValume() {
        return super.getStorageValume();
    }

    public String getModel() {
        return super.getModel();
    }

    public String getName() {
        return super.getName();
    }

    public void setModel() {
        super.setModel();
    }

    public void setName() {
        super.setName();
    }

    public void setRAMvolume() {
        super.setRAMvolume();
    }

    public void setStorageVolume() {
        super.setStorageVolume();
    }

    public String getVersia() {
        return versia;
    }

    public void setVersia(String versia) {
        this.versia = versia;
    }

    public void call() {
        System.out.println("Write number");
    }

    public void sandMessage() {
        System.out.println("Text message");
    }

    public void makePhoto() {
        System.out.println("Click");
    }

    public void makeVideo() {
        System.out.println("Start video");
    }

    public String toString() {
        return "Samsung phone " + versia;
    }
}